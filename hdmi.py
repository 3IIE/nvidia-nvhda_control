#!/usr/bin/env python3
''' This script is a controller to for the kernel module nvhda. nvhda enables
audio through HDMI on nvidia notebook graphics cards. I have no affiliation with
the writers of nvhda kernel module. 

This module can install/uninstall nvhda directly from 
github: https://github.com/hhfeuer/nvhda.

    Install nvhda kernel module using make:
        sudo hdmi.py -install

    Install nvhda kernel module with dkms using:
        sudo hdmi.py -install dkms

    Uninstall: This script will make a config file that remembers your install choice
    make or with dkms so you dont have to remember which install method you used for
    install.
        sudo hdmi.py -uninstall

    Update: This script can update the nvhda kernel module. It is not smart enough to
    know if an update is available though. It will update using the same method used
    during initial install; with or without dkms.
        sudo hdmi.py -update

Using this script to control nvhda kernel module.

    Check nvhda module status:
        sudo hdmi.py

    Start nvhda module to enable audio through HDMI:
      If module is not loaded it will be loaded then enabled.
        sudo hdmi.py on

    Stop nvhda module:
        sudo hdmi.py off

 '''
 
import os
import subprocess as subP
import sys
from zipfile import ZipFile as ziper
from shutil import rmtree, chown
import json

class hdmiCtrl():
    ''' Class controls the HDMI sound card functionality on a NVIDIA 1070. '''

    def __init__(self, *args, **kargs):
        ''' Builds the controls for HDMI sound card. '''

        self.on = ['ON', 'on']
        self.off = ['OFF', 'off']
        self.nvhdaLoc = os.path.join(os.path.expanduser('/'), 'proc', 'acpi', 'nvhda')

        ## run a argument checker for module setup controls.
        if args != ():
            modManager(args[0])

            if True in [True for x in self.on if x in args[0]]:
                self.start(self.isUp())

            elif True in [True for x in self.off if x in args[0]]:
                self.stop(self.isUp())

        else:
            __isup = self.isUp()
            if __isup is False:
                print('nvhda hdmi control module not loaded.\nTo load module turn it on.')
            else:
                print(f'HDMI is {__isup}.')


    def start(self, __isup):
        ''' Starts the HDMI sound card. '''
        if __isup is False or __isup is None:
            ## look for this > Module nvhda not found < to test if nvhda is installed.
            try:
                loadCmd = ['sudo', 'modprobe', 'nvhda']
                subP.Popen(loadCmd, stdin=subP.PIPE, stdout=subP.PIPE).communicate()
                with open(self.nvhdaLoc, 'w') as iFile:
                    iFile.write(self.on[0])
            except PermissionError:
                print('You need to run as root.')
        else:
            if __isup == self.off[0]:
                try:
                    with open(self.nvhdaLoc, 'w') as iFile:
                        iFile.write(self.on[0])
                except PermissionError:
                    print('You need to run as root.')


    def stop(self, __isup):
        ''' Stops the HDMI sound card. '''
        if __isup is not False and self.on[0] in __isup:
            try:
                with open(self.nvhdaLoc, 'w') as iFile:
                    iFile.write(self.off[0])
            except PermissionError:
                print('You need to run as root.')


    def isUp(self):
        ''' Class checks status of HDMI module and returns status ['ON' or 'OFF'] or 
        False if module is not loaded. '''
        try:
            with open(self.nvhdaLoc, 'r') as iFile:
                status = iFile.read().rstrip()
        except FileNotFoundError as e:
            return False
        else:
            return status[-3:].lstrip()


class modManager():
    ''' Class handles the nvhda github download and installes/uninstalls the module
        nvhda installer in make and with dkms. '''
    def __init__(self, args):
        ''' Builds self based on args: install, uninstall and update. '''
        self.user, installPath = userInstallPathSetup()
        self.dwnLoc = os.path.join(installPath, 'nvhda')
        self.curDir = os.getcwd()

        if '-install' in args:

            self.get()
            if args[-1] != '-install' and args[args.index('-install') + 1] == 'dkms':
                self.makeDKMS()
            else:
                self.make()

            ## recursively set the ownership of our install location back to our $USER.
            chown(self.dwnLoc, self.user, self.user)
            for pw, dr, fl in os.walk(self.dwnLoc):
                for d in dr:
                    chown(os.path.join(pw, d), self.user, self.user)
                for f in fl:
                    chown(os.path.join(pw, f), self.user, self.user)

        if '-uninstall' in args:

            cfg = config(**{'baseDir': self.dwnLoc})
            if cfg.read() is not False:
                if cfg.read()['dkms'] is True:
                    self.makeDKMS(['-uninstall'])
                else:
                    self.make(['-uninstall'])
            elif args[-1] != '-uninstall' and args[args.index('-uninstall') + 1] == 'dkms':
                self.makeDKMS(['-uninstall'])
            else:
                self.make(['-uninstall'])

        if '-update' in args:
            
            cfg = config(**{'baseDir': self.dwnLoc})
            cfgRead = cfg.read()
            if cfgRead is not None and cfgRead is not False:
                if cfgRead['dkms'] is True:
                    self.makeDKMS(['-uninstall'])
                else:
                    self.make(['-uninstall'])
                rmtree(self.dwnLoc)
                self.get()
                if cfgRead['dkms'] or args[-1] != '-update' and args[args.index('-update') + 1] == 'dkms':
                    self.makeDKMS()
                else:
                    self.make()

        
    def make(self, marg=None):
        ''' Function handles the installation and uninstallation of nvhda using make. '''
        marg = marg if marg is not None else []
        nvLoc = os.path.join(self.dwnLoc, 'nvhda-master')

        if '-uninstall' not in marg:
            if os.path.exists(nvLoc):
                os.chdir(nvLoc)
                cmds = [['make'], ['sudo', 'make', 'install']]
                for cmd in cmds:
                    cmdReadOut(cmd)
                os.chdir(self.curDir)
                config(**{'baseDir': self.dwnLoc, 'dkms': False})
        else:
            if os.path.exists(nvLoc):
                os.chdir(nvLoc)
                cmd = ['sudo', 'make', 'uninstall']
                cmdReadOut(cmd)

            os.chdir(self.curDir)


    def makeDKMS(self, darg=None):
        ''' Function handles the installation and uninstallation of nvhda using dkms. '''
        darg = darg if darg is not None else []
        nvLoc = os.path.join(self.dwnLoc, 'nvhda-master')

        if '-uninstall' not in darg:
            if os.path.exists(nvLoc):
                os.chdir(nvLoc)
                cmd = ['sudo', 'make', '-f', 'Makefile.dkms']
                cmdReadOut(cmd)
                config(**{'baseDir': self.dwnLoc, 'dkms': True})
        else:
            if os.path.exists(nvLoc):
                os.chdir(nvLoc)
                cmd = ['sudo', 'make', '-f', 'Makefile.dkms', 'uninstall']
                cmdReadOut(cmd)

            os.chdir(self.curDir)


    def get(self, dest=''):
        ''' Function will download and unzip the nvhda module from github. '''

        dest = dest if dest != '' and os.path.abspath(dest) else self.dwnLoc

        if not os.path.exists(dest):
            os.mkdir(dest)
            os.chdir(dest)
            git_nvhda = 'https://github.com/hhfeuer/nvhda/archive/master.zip'
            cmdReadOut(['wget', git_nvhda])

            with ziper('master.zip', 'r') as zipF:
                zipF.extractall(self.dwnLoc)
            os.remove('master.zip')


class config():
    ''' Class creates and controls the config file. '''
    def __init__(self, **kargs):
        ''' Function sets up the config file location. '''
        baseDir = kargs['baseDir']

        if os.path.exists(baseDir):
            self.jsonPath = os.path.join(baseDir, 'config.json')
        else:
            sys.exit('File system not setup.')

        if isinstance(kargs, dict):
            self.write(kargs)


    def read(self):
        ''' Function returns the config file as an object. '''
        if os.path.exists(self.jsonPath):
            try:
                with open(self.jsonPath, 'r') as cfgFile:
                    return json.load(cfgFile)
            except Exception as e:
                print(f'Cannot open {self.jsonPath}\n{e}')
                return False

    def write(self, setting):
        ''' Function writes/edits the config file. '''
        if not os.path.exists(self.jsonPath):
            with open(self.jsonPath, 'w') as jOpen:
                jOpen.write(json.dumps({x: y for x, y in setting.items() if x != 'baseDir'}, indent=4))
        else:
            jData = self.read()
            with open(self.jsonPath, 'w') as jOpen:

                for key, val in {x: y for x, y in setting.items() if x != 'baseDir'}.items():
                    if key not in jData.keys():
                        jData.update(key, val)
                    else:
                        jData[key] = val

                jOpen.write(json.dumps(jData, indent=4))


def userInstallPathSetup():
    ''' Create the file location for install if it does not exist and return $USER. '''
    appDir = os.path.join(os.path.expanduser('~'), '.local', 'share', 'applications')
    if not os.path.exists(appDir):
        os.mkdir(appDir)
    return (os.path.split(os.path.expanduser('~'))[1], appDir)



def cmdReadOut(cmd):
    ''' Read progress in real time to stdout and log output for return. '''
    process = subP.Popen(cmd,
                         bufsize=1,
                         universal_newlines=True,
                         stdout=subP.PIPE,
                         stderr=subP.STDOUT)
    outPut = ''
    for line in iter(process.stdout.readline, ''):
        if line != '':
            outPut = f'{outPut}\n{line}'
            sys.stdout.buffer.write(line.replace('\n', '\r').encode())
        sys.stdout.flush()
    process.wait()
    errcode = process.returncode
    return (outPut, True) if errcode == 0 else (outPut, errcode)


if __name__ == '__main__':
    
    if len(sys.argv) > 1:
        hdmiCtrl(sys.argv[1:])
    else:
        hdmiCtrl()
