

This module is a controller to for the kernel module nvhda. nvhda enables
audio through HDMI on nvidia 1070 graphics cards on notebook computers. 

I have no affiliation with the writers of nvhda kernel module. This script
is basically just a python wrapper for nvhda to simplify its use.

This module can install/uninstall nvhda directly from 
github: https://github.com/hhfeuer/nvhda.

If you want to know more about the nvhda kernel module follow the github link above.

## Install/uninstall operations:

### Install the hdmi.py script. 

1. Download or copy the hdmi.py script.

2. It worKs best in /usr/local/sbin. This way you wont have to change directories to run this script. If you don't place it there you will have to preface all commands with python3.
```bash
## example
sudo python3 hdmi.py [opperation]
```

Finally make file executable with root permissions:
```bash
sudo chown root:root /usr/local/sbin/hdmi.py
sudo chmod +x /usr/local/sbin/hdmi.py
```

#### Install nvhda kernel module using make:
```bash
sudo hdmi.py -install
```

#### Install nvhda kernel module with dkms using:
```bash
sudo hdmi.py -install dkms
```

#### Uninstall: This script will make a config file that remembers your install choice make or with dkms so you dont have to remember which install method you used for install.
```bash
sudo hdmi.py -uninstall
```

#### Update the nvhda module from github. This does not update this script but rather the kernel module itself. This script will update with the same install method used for the initial install; with or without dkms installation.
```bash
sudo hdmi.py -update
```

## Using this script to control nvhda kernel module.

#### Check nvhda module status:
```bash
sudo hdmi.py
```

#### Start nvhda module to enable audio through HDMI. If module is not loaded it will be loaded.
```bash
sudo hdmi.py on
```

#### Stop nvhda module to disable audio through HDMI:
```
sudo hdmi.py off
```